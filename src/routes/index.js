var util = require('util');
const minimatch = require('minimatch');

module.exports = function (app, addon) {
    //healthcheck route used by micros to ensure the addon is running.
    app.get('/healthcheck', function(req, res) {
        res.send(200);
    });

    // Root route. This route will redirect to the add-on descriptor: `atlassian-connect.json`.
    app.get('/', function (req, res) {
        res.format({
            // If the request content-type is text-html, it will decide which to serve up
            'text/html': function () {
                res.redirect('/atlassian-connect.json');
            },
            // This logic is here to make sure that the `atlassian-connect.json` is always
            // served up when requested by the host
            'application/json': function () {
                res.redirect('/atlassian-connect.json');
            }
        });
    });

    app.get('/configure', addon.authenticate(), function (req, res) {

        console.log(req.query);
        addon.settings.get('task', req.query.repoId).then((result) => {
            res.render('manage-tasks', {
                title: 'Manage Default Tasks',
                repoId: req.query.repoId,
                tasks: result ? JSON.stringify(result.tasks) : ''
            });
        });

    });

    app.put('/update-task', addon.checkValidToken(), function (req, res) {
        const tasksArray = JSON.parse(req.body.content);
        if (!Array.isArray(tasksArray)) {
            console.log('Bad data received. Request body: ' + JSON.stringify(req.body));
            res.sendStatus(400);
            return;
        }
        const dataToSave = {
            tasks: tasksArray
        };
        const repoId = req.query.repoId;

        addon.settings.set('task', dataToSave, repoId).then(() => {
            console.log(`Saved task info ${JSON.stringify(dataToSave)} for repo ${repoId}`);
            res.sendStatus(200);
        }).catch((error) => {
            console.log(error);
            console.log(`Failed to save task info ${JSON.stringify(dataToSave)} for repo ${repoId}`);
            res.sendStatus(500);
        })
    });


    app.post('/uninstalled', addon.authenticate(), function(req, res) {
        console.log('uninstalled!');
        res.sendStatus(200);
    });

    app.post('/webhook', addon.authenticate(), function (req, res) {
        const pullRequestId = req.body.data.pullrequest.id;
        const repoFullName = req.body.data.repository.full_name;
        const destinationBranch = req.body.data.pullrequest.destination.branch.name;
        addon.settings.get('task', req.body.data.repository.uuid).then((repoTaskInfo) => {
            const tasks = repoTaskInfo && repoTaskInfo.tasks;
            const matchingTasks = tasks.filter(({branch = ''}) => {
                return !branch || minimatch(destinationBranch, branch);
            });
            if (matchingTasks.length) {
                var httpClient = addon.httpClient(req);
                httpClient.post({
                    url: `/!api/1.0/repositories/${repoFullName}/pullrequests/${pullRequestId}/comments/`,
                    json: {
                        'content': `Default tasks for this repository:`
                    }
                }, (addCommentError, addCommentHttpResponse, addCommentResponseBody) => {
                    if (addCommentError) {
                        console.error('Posting comment failed! ', addCommentError);
                        res.sendStatus(500);
                    } else if (addCommentHttpResponse.statusCode >= 400) {
                        console.error('Posting comment failed! Received response ', addCommentHttpResponse.statusCode, addCommentResponseBody);
                        res.sendStatus(500);
                    } else {
                        console.log('Comment posted! id: ' + addCommentResponseBody.comment_id);

                        matchingTasks.forEach((task) => {
                            httpClient.post({
                                // NOTE: this is an internal API and will probably break at some point in the future!
                                // There is currently no public API for adding a task to a PR
                                url: `/!api/internal/repositories/${repoFullName}/pullrequests/${pullRequestId}/tasks/`,
                                json: {
                                    "content": {
                                        "raw": task.text
                                    },
                                    "comment": {
                                        "id": addCommentResponseBody.comment_id
                                    },
                                    "completed": false,
                                    "state": "UNRESOLVED"
                                }
                            }, (addTaskError, addTaskHttpResponse, addTaskResponseBody) => {
                                if (addTaskError) {
                                    console.error('Creating task failed: ', addTaskError);
                                    res.sendStatus(500);
                                } else if (addTaskHttpResponse.statusCode >= 400) {
                                    console.error('Creating task failed: Received response ', addTaskHttpResponse.statusCode, addTaskResponseBody);
                                    res.sendStatus(500);
                                } else {
                                    console.log('Task created! id: ' + addTaskResponseBody.id);
                                }
                            });
                        });
                    }
                });
            }
        });
        res.sendStatus(204);
    });
};
